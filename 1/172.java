class Robot {
    public int getAge(){
        int age = 5;
        return age;
    }
}

class Story {
	public static void main(String[] args) {
		Robot robin = new Robot();
		System.out.println("Robin ist " + robin.getAge() + " Jahre alt.");
	}
}
