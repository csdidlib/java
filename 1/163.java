class Robot {
    int age = 1;
       void sayAge() {
           System.out.println("Ich bin " + age + " Jahr alt.");
    }
}

class Story {
    public static void main(String[] args) {
        Robot robin = new Robot();
        robin.sayAge();
    }
}
