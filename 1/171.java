class ReadingRobot {
    String task = "Ich lese codierte Nachrichten.";
	void sayTask() {
		System.out.println(task);
	}
	public String getTask() {
	    return task;
	}
}

class Robot {
    String task = "Ich infiltriere die Geheimbasis und suche Paco.";
    void sayTask() {
		System.out.println(task);
	}
	String getTask() {
		return task;
	}
}

class Story {
    public static void main(String[] args) {
		Robot robin = new Robot();
		ReadingRobot alex = new ReadingRobot();          
		System.out.println(robin.getTask());
		System.out.println(alex.getTask());
    }
}
