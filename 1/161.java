class Robot {
	void sayHello() {
		System.out.println("Hallo.");
	}
	void sayWelcome() {
		System.out.println("Willkommen.");
	}
}

class Story {
	public static void main(String[] args) {
		Robot robin = new Robot();
		Robot alex  = new Robot();
		robin.sayHello();
        alex.sayWelcome();
	}
}
