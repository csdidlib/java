class ReadingRobot {
	void giveTask() {
		System.out.println("Ich kann lesen.");
	}
}

class Robot {
    void giveTask() {
        System.out.println("Ich infiltriere die Geheimbasis und suche Paco.");
    }
}

class Story {
    public static void main(String[] args) {
        Robot robin = new Robot();
        ReadingRobot bookworm = new ReadingRobot();
        robin.giveTask();
        bookworm.giveTask();
    }
}
