public class MathRobot {
	public double div(String a, String b) {
	    return (double) Integer.parseInt(a) / Integer.parseInt(b);
	}
}

public class Story {
	public static void main(String args[]) {
		MathRobot ronja = new MathRobot();
		System.out.println("3 durch 2 ist: " + ronja.div("3","2"));
		System.out.println("2 durch 2 ist: " + ronja.div("2","2"));
	}
}
