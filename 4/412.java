public class Robot {

	private String name;
	private int age;
	private double batteryRuntime;

	Robot(String name, int age, double batteryRuntime) {
		this.name = name;
		this.age = age;
		this.batteryRuntime = batteryRuntime;
	}
	
	public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Robot robot = (Robot) obj;
        return age == robot.age &&
                Double.compare(robot.batteryRuntime, batteryRuntime) == 0 &&
                name.equals(robot.name);
    }
}

public class Story {

	public static void main(String[] args) {
		Robot ronja = new Robot("Ronja", 5, 3.5);
		Robot robin = new Robot("Robin", 4, 2.5);
		Robot rob = new Robot("Ronja", 5, 3.5);
		System.out.println(ronja.equals(robin));
		System.out.println(ronja.equals(rob)); 
	}
}
