class Robot {
    public void testNumber(int x){
        if (x % 2 == 0) {
            System.out.println("Die Zahl ist durch 2 teilbar!");
        }
        else {
            System.out.println("Die Zahl ist nicht durch 2 teilbar!");
        }
    }
}

class Story {
	public static void main(String[] args) {
		int number = 2;
		Robot robin = new Robot();
		robin.testNumber(number);
	}
}
