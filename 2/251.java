class Robot {

    String clue1 = "Boesewicht mag keine Papageien.";
    String clue2 = "Boesewicht kennt sich mit Computern aus.";
    String clue3 = "E-Mail des Boesewichts.";

    String[] clues = new String[3];

    void saveClues(){
        clues[0] = clue1;
        clues[1] = clue2;
        clues[2] = clue3;

    }

}

class Story {

	public static void main(String[] args) {
		Robot robin = new Robot();
		robin.saveClues();
		for (int i = 0; i < 3; i++) {
            System.out.println(robin.clues[i]);
        }
	}
}