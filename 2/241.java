class Robot {
    void countTo(int number){
        for (int i = 1; i <= number ; i++) {
            System.out.println(i);
        }
    }
}

class Story {
   public static void main(String args[]) {
        Robot robin = new Robot();
        robin.countTo(20);
    }
}
