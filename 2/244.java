class Robot {
    EncryptedEMail encryptedEMail = new EncryptedEMail();

    public void decryptSender() {
        int rows = 4; 
        int columns = 28;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                char decryptedChar = encryptedEMail.decryptChar(i, j);
                System.out.print(decryptedChar);
            }
            System.out.println();
        }
    }
}

class Story {

    public static void main(String[] args) {
        Robot robin = new Robot();
        robin.decryptSender();
    }

}
