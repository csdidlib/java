class Robot {
    public String completeName(String n, String m){
        String s;
        s = n + m;
        return s;
    }
}

class Story {
	public static void main(String args[]) {
		Robot robin = new Robot();
		System.out.println("Robins Name ist " + robin.completeName("Robin", "Suchroboter"));
	}
}
