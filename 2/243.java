import java.util.Random;

class RandomNumber {
    SecretRandomNumber rand;
    public RandomNumber() {
        rand = new SecretRandomNumber();
    }
    int giveNumber() {
        return rand.giveNumber();
    }
}

class RandomNumberGreaterTen {
    RandomNumber rand = new RandomNumber();
    public static int greaterTen(){
        int x = 0;
        while (x <= 10){
            RandomNumber n = new RandomNumber();
            x = n.giveNumber();          
        }
        return x;
    }
}

class Story {
	public static void main(String[] args) {
		RandomNumberGreaterTen numberGreaterTen = new RandomNumberGreaterTen();
		for (int i=0; i<5; i++) {
			System.out.println(numberGreaterTen.greaterTen());
		}
	}
}
