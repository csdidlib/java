class Robot {
    public void speakTwice(String n){
        System.out.println(n);
        System.out.println(n);
    }
}

class Story {
	public static void main(String[] args) {
		Robot robin = new Robot();
		robin.speakTwice("Hallo, ich bin Robin.");
	}
}
