class Robot {
    String[] clues = {
        "Boesewicht mag keine Papageien.", 
        "Boesewicht kennt sich mit Computern aus.", 
        "E-Mail-Adresse des Boesewichts."
    };

    public String giveClue(int index) {
        return clues[index];
    }

    public void printClues() {
        for (int i = 0; i < clues.length; i++) {
            System.out.println(giveClue(i));
        }
    }
}

class Story {

	public static void main(String[] args) {
		Robot robin = new Robot();
        robin.printClues();
	}
}