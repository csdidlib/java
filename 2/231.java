class Robot {
	int batteryRuntime = 5;
	boolean isBatteryRuntimeLow() {
		if (batteryRuntime < 2) {
            return true;
		} else {
            return false;
    	}
    }
}

class Story {
	public static void main(String[] args) {
		Robot robin = new Robot();
		System.out.println(robin.isBatteryRuntimeLow());
	}

}
