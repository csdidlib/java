class Robot {
    boolean hasTask;
    boolean batteryEmpty;
    int taskCounter;

    void helpDuke() {
        // In dem nachfolgenden if-Konstrukt fehlen die booleschen Ausdrücke, die Attributänderungen 
        // und die Ausgabetexte. Ergänze die Bedingungen, füge die notwendigen Statements hinzu und
        // ergänze die Ausgaben "Das erledige ich gern für dich.", 
        // "Duke! Ich brauch Mal ne Pause... Frag mich später nochmal!", "Ich hab gerade nichts zu tun."
        // und "Ich lade meine Batterie." an geeigneter Stelle.
        
    	if (hasTask = true && taskCounter < 3) {
    	    taskCounter =+ 1;
    		System.out.println("Das erledige ich gern für dich.");
    	} else if (taskCounter == 3) {
    		System.out.println("Duke! Ich brauch Mal ne Pause... Frag mich später nochmal!");
    		taskCounter = 0;
    	} else if (hasTask = false) {
    	    System.out.println("Ich hab gerade nichts zu tun.");
    	} else {
    	    batteryEmpty = true;
    		System.out.println("Ich lade meine Batterie.");
    	}
    }
}

class Story {
    /* Folgende Ausgabe soll erzeugt werden:
    * Ich hab gerade nichts zu tun.
    * Das erledige ich gern für dich.
    * Das erledige ich gern für dich.
    * Ich lade meine Batterie.
    * Das erledige ich gern für dich.
    * Duke! Ich brauch Mal ne Pause... Frag mich später nochmal!
    * Das erledige ich gern für dich.
    * Das erledige ich gern für dich. */

    public static void main(String[] args) {
        Robot robin = new Robot();
        
        robin.hasTask = false;
        robin.batteryEmpty = false;
        robin.taskCounter = 0;
        
        // Robin hat keine Aufgabe und kann Duke daher nicht helfen
        robin.helpDuke();
        
        // Robin hat eine Aufgabe und hilft Duke 2 mal
        robin.hasTask = true;
        robin.helpDuke();
        robin.helpDuke();
        
        // Robins Batterie ist alle, er lädt sich auf
        robin.batteryEmpty = true;
        robin.helpDuke();
        
        // Robins Batterie ist wieder voll. Er hilft Duke noch ein paar mal und macht zwischendrin Pause
        robin.helpDuke();
        robin.helpDuke();
        robin.helpDuke();
        robin.helpDuke();
    }
}
