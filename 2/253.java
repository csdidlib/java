class Email {
    static String[][] ipaddress = HiddenIPAddress.getIPAddress();
}

class Robot {

    public void identifyIP() {
        for (int i = 0; i < Email.ipaddress.length; i++) {
            for (int j = 0; j < Email.ipaddress[i].length; j++) {
                if (!Email.ipaddress[i][j].equals("x")) {
                    System.out.println(Email.ipaddress[i][j]);
                }
            }
        }
    }
}

class Story {

    public static void main(String[] args) {
        Robot robin = new Robot();
        robin.identifyIP();
    }
}