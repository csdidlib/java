public class Robot {
	
	boolean xor(boolean a, boolean b){
		if (a == true && b == true){
			return false;
		} else if (a == false && b == false) {
			return false;
		} else {
			return true;
		}	
	}
}

public class Story {
	
	public static void main(String[] args) {
		Robot robin = new Robot();
		System.out.println("xor von true und true ergibt " + robin.xor(true,true));
		System.out.println("xor von true und false ergibt " + robin.xor(true,false));
		System.out.println("xor von false und true ergibt " + robin.xor(false,true));
		System.out.println("xor von false und false ergibt " + robin.xor(false,false));
	}
	
}