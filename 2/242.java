import java.util.Random;

class RandomNumber {
    SecretRandomNumber rand;
    public RandomNumber() {
        rand = new SecretRandomNumber();
    }
    int giveNumber() {
        return rand.giveNumber();
    }
}

class Robot {
    String sentence = "Ich kann sprechen!";
    RandomNumber rand = new RandomNumber();
    void speakSeveralTimes(int i){
        int j;
        for (j = 1; j <= i; j++) {
            System.out.println(sentence);
            }
    }
    void speak(){
        int x;
        RandomNumber n = new RandomNumber();
        x = n.giveNumber();
        speakSeveralTimes(x);
    }
}

class Story {
   public static void main(String args[]) {
        Robot robin = new Robot();
        robin.speak();
    }
}
