class Animal {

	void move() {
		System.out.println("Ich bewege mich");
	}
}

class Parrot extends Animal {

    @Override
    void move() {
        System.out.println("Ich fliege");
    }

	void speak() {
	    System.out.println("Ich kann sprechen");
	}
	
	void speak(String s) {
		System.out.println(s);
	}

}

class Story {

	public static void main(String[] args) {
		Parrot paco = new Parrot();
		paco.move();
		paco.speak();
		paco.speak("Ich kann viele verschiedene Sätze sagen");
	}
}