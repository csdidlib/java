class Story {

	public static void main(String args[]) {
		TestRobot tr = new TestRobot();
		Terminal terminal = new Terminal();
		terminal.hackRobot(tr);
	}

}

class Terminal {
    public void hackRobot(TestRobot x) {
        System.out.println("numberOfProcessorCores: " + x.numberOfProcessorCores);
        System.out.println("hasFirewall: " + x.hasFirewall);
        System.out.println("id: " + x.id);
    }
}

class TestRobot {
	private int secretKey = 602413;
	protected int numberOfProcessorCores = 4;
	boolean hasFirewall = false;
	public String id = "58-08-2";
}

