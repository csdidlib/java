class Robot {
	private String internalStorage = "416e737765723f203432";
    
    private void flushStorage() {
        internalStorage = "";
    }
    
    public void safeFlushStorage(String s) {
        if (s == "pw_duke") {
            System.out.println(internalStorage);
            flushStorage();
        }
    }

}

class Story {
	public static void main(String args[]) {
		Robot ronja = new Robot();
		ronja.safeFlushStorage("wrong_pw");
		ronja.safeFlushStorage("pw_duke");
	}
}