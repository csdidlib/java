class DetectiveRobot extends Robot {
    int spareBatteryRuntime = 5;

    int giveSpareBatteryRuntime() {
        return spareBatteryRuntime;
    }

    int giveTotalBatteryRuntime() {
        return giveBatteryRuntime() + giveSpareBatteryRuntime();
    }
}

class Robot {
	int batteryRuntime = 10;

	int giveBatteryRuntime() {
		return batteryRuntime;
	}
}

class Story {
	public static void main(String args[]) {
		Robot rob = new Robot();
		DetectiveRobot ronja = new DetectiveRobot();
		System.out.println("Die normale Laufzeit beträgt " + rob.giveBatteryRuntime() + " Stunden.");
		System.out.println("Ronjas Laufzeit beträgt " + ronja.giveTotalBatteryRuntime() + " Stunden.");
	}
}