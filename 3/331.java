class Robot {
	private int batteryRuntime = 0;

    private boolean notNegative(int x) {
        return x >= 0; 
    }

	public void setBatteryRuntime(int newTime) {
	    if (notNegative(newTime)) {
		    batteryRuntime = newTime;
		    }
	}

	public int getBatteryRuntime() {
		return batteryRuntime;
	}
}

class Story {

	public static void main(String[] args) {
		Robot ronja = new Robot();
		ronja.setBatteryRuntime(-1);
		System.out.println(ronja.getBatteryRuntime());
		ronja.setBatteryRuntime(5);
		System.out.println(ronja.getBatteryRuntime());
	}
}
