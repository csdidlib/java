public class Robot {
	
	String name = "Robin";
	int batteryRuntime = 10;
	String color = "grau";
	
	public Robot(String name){
		this.name = name;
	}

    public Robot(String name, int batteryRuntime) {
        this.name = name;
        this.batteryRuntime = batteryRuntime;
    }
    
    public Robot(String name, int batteryRuntime, String color) {
        this.name = name;
        this.batteryRuntime = batteryRuntime;
        this.color = color;
    }
    
	public String getName() {
		return name;
	}

	public int getBatteryRuntime() {
		return batteryRuntime;
	}

	public String getColor() {
		return color;
	}

}

public class Story {
	
	public static void main(String[] args) {
		Robot robin = new Robot("Robin");
		Robot ronja = new Robot("Ronja", 8);
		Robot rolf = new Robot("Rolf", 20, "blau");
		
		System.out.println("Robot " + robin.getName() + " hat " + robin.getBatteryRuntime() + "h Laufzeit und ist " + robin.color);
		System.out.println("Robot " + ronja.getName() + " hat " + ronja.getBatteryRuntime() + "h Laufzeit und ist " + ronja.color);
		System.out.println("Robot " + rolf.getName() + " hat " + rolf.getBatteryRuntime() + "h Laufzeit und ist " + rolf.color);
	}

}
