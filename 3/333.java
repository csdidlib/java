class Robot {
    private int batteryRuntime = 2;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName;
    }

    public int getBatteryRuntime() {
        return batteryRuntime;
    }

    void rechargeBattery() {
        batteryRuntime = 10;
    }

    public void saySomething() {
        System.out.println(createSentence());
    }

    private String createSentence() {
        return "Meine Batterie reicht " + batteryRuntime + " Stunden.";
    }
}

class Story {
    public static void main(String[] args) {
        Robot ronja = new Robot();
        ronja.setName("Ronja");
        System.out.println("Ich bin " + ronja.getName());
        ronja.saySomething();
        
        if (ronja.getBatteryRuntime() < 3) {
            ronja.rechargeBattery();
            System.out.println("Nach dem Laden reicht meine Batterie " + ronja.getBatteryRuntime() + " Stunden.");
        }
    }
}
