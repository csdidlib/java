public class MathRobot {

    public int addition(int a, int b) {
        return a + b;
    }

    public int addition(int a, int b, int c) {
        return a + b + c;
    }

    public double addition(double a, double b) {
        return a + b;
    }
    public double addition(double a, double b, double c) {
        return a + b + c;
    }
}

class Story {

	public static void main(String[] args) {
		MathRobot mathRobot = new MathRobot();
		System.out.println(mathRobot.addition(2, 3));
		System.out.println(mathRobot.addition(2, 3, 5));
		System.out.println(mathRobot.addition(2.0, 3.2));
		System.out.println(mathRobot.addition(2.0, 3.2, 0.6));
	}

}
