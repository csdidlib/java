class Robot {
	private boolean activated = false;

    public void setActivated(boolean x) {
        activated = x;        
    }
    
    public boolean isActivated() {
        return activated;    
    }

	public void printStatus() {
		System.out.println(activated);
	}
}

class Story {
	public static void main(String args[]) {
		Robot ronja = new Robot();
		ronja.setActivated(true);
		System.out.println(ronja.isActivated());
		ronja.printStatus();
		ronja.setActivated(false);
		System.out.println(ronja.isActivated());
		ronja.printStatus();
	}
}
