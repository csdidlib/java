public class OldRobot {
	int internalStorageSize = 0;

	void setStorage(int storageSize) {
		this.internalStorageSize = storageSize;
	}
}

public class Robot extends OldRobot {
	int additionalStorageSize = 0;
	
	void setStorage(int storageSize) {
	    if (storageSize <= 1000) {
	        super.setStorage(storageSize);    
	    } else {
	        super.setStorage(1000);
	        additionalStorageSize = storageSize - 1000;
	    }
	}
}

class Story {

	public static void main(String[] args) {
        Robot ronja = new Robot();
        ronja.setStorage(1250);
        System.out.println(ronja.internalStorageSize);
        System.out.println(ronja.additionalStorageSize);
    }

}
