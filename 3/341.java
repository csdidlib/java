class OldRobot {
	void greetUser() {
		System.out.println("Hallo User!");
	}

	int getInternalStorageSize() {
		return 5;
	}
}

class Robot extends OldRobot {
	int internalStorageSize = 7;

	void greetUser() {
		System.out.println("Hallo Duke!");
	}

	int getInternalStorageSize() {
		return internalStorageSize;
	}	

}

class Story {
	public static void main(String[] args) {
		OldRobot robin = new OldRobot();
		Robot ronja = new Robot();
		robin.greetUser();
		ronja.greetUser();
		System.out.println(robin.getInternalStorageSize());
		System.out.println(ronja.getInternalStorageSize());
	}
}
