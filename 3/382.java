class ReadingRobot extends Robot {
    ReadingRobot() {
        super(5);
    }

}

abstract class Robot {

	private int batteryRuntime;

	Robot(int batteryRuntime) {
		this.batteryRuntime = batteryRuntime;
	}
	
	public int getBatteryRuntime() {
	    return batteryRuntime;
	}

}

class Story {
	
	public static void main(String[] args) {
		ReadingRobot bookworm = new ReadingRobot();
		System.out.println(bookworm.getBatteryRuntime());
	}
}