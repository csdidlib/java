lass DetectiveRobot extends Robot {

    String informant = "geheimer Informant";

	public DetectiveRobot() {
	    name = "Ronja";
	    destination = "Eikes Geheimquartier";
	    changeBatteryRuntime(8);
	}

	String questioningInformant() {
		 return "Ich bewege mich nach " + destination + "\nund befrage dort " + informant + ".";
	}
	
	@Override
	String robotDetails() {
	    String infoString = "Meine Laufzeit beträgt " + batteryRuntime + " Stunden.\n";
        infoString += "Meine aktuelle Aufgabe: " + questioningInformant();
        
        return infoString;
    }

}

class FlyingDetectiveRobot extends DetectiveRobot {

    boolean isFlying;

	public FlyingDetectiveRobot() {
	    name = "Rolf";
	    destination = "kurz vor den Wolken";
	    isFlying = true;
	}
	
	@Override
	String robotDetails() {
	    String infoString = "Meine Laufzeit beträgt " + batteryRuntime + " Stunden. Ich kann fliegen.\n";
	    if (isFlying) {
            infoString += "Momentan fliege ich. Ich lande jetzt. ";
            toggleFlyingMode();
            infoString += "Gelandet.\n";
        } else {
            infoString += "Ich fliege nicht. Ich schalte Fliegen jetzt ein. ";
            toggleFlyingMode();
            infoString += "Fliegen Gestartet.\n";
        }
        
        infoString += "Meine aktuelle Aufgabe: " + questioningInformant();
        return infoString;
    }

    void toggleFlyingMode() {
        isFlying = !isFlying;
    }
    
}

class Robot {
	int batteryRuntime;
	String name;
	String destination;
	
	public Robot() {
	    name = "Robin";
	    destination = "Dukes Wohnung";
	    batteryRuntime = 2;
	}

    void changeBatteryRuntime(int newBatteryRuntime) {
        batteryRuntime = newBatteryRuntime;
    }
    
    String robotDetails() {
        return "Meine Laufzeit beträgt " + batteryRuntime + " Stunden.";
    }
	
	void printInfo() {
	    System.out.println("Informationen zum Roboter:");
	    System.out.println("Ich bin " + name + ".");
	    System.out.println(robotDetails());
	    System.out.println("Mein Ziel ist " + destination + ".");
	    System.out.println("---");
	}
}

class Story {

	public static void main(String[] args) {
		System.out.println("Die Superklasse von Robot ist " + Robot.class.getSuperclass());
		System.out.println("Die Superklasse von DetectiveRobot ist " + DetectiveRobot.class.getSuperclass());
		System.out.println("Die Superklasse von FlyingDetectiveRobot ist " + FlyingDetectiveRobot.class.getSuperclass());
		
		/*
		Robot robin = new Robot();
		DetectiveRobot ronja = new DetectiveRobot();
		FlyingDetectiveRobot rolf = new FlyingDetectiveRobot();
		
		System.out.println();
		robin.printInfo();
		ronja.printInfo();
		rolf.printInfo();
		*/
	}

}