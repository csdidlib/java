abstract public class AbstractLock {
    int secretNumber;
    String secretWord;
    int numberOfAttempts;
    abstract boolean openWithSecretNumber(int number, int attempt);
    abstract boolean openWithSecretWord(String word);
}

class Lock1 extends AbstractLock {
	Lock1(int secretNumber, String secretWord) {
		this.secretNumber = secretNumber;
		this.secretWord = secretWord;
	}

    @Override
	boolean openWithSecretNumber(int number, int attempt) {
		if (number == secretNumber && attempt < numberOfAttempts) {
			return false;
		}
		else {
		    return true;
		}
	}

	@Override
	boolean openWithSecretWord(String word) {
		if (word.equals(secretWord)) {
			return true;
		}
		else {
		    return false;
		}
	}
}

class Lock2 extends AbstractLock {
	Lock2(int secretNumber, String secretWord) {
		this.secretNumber = secretNumber;
		this.secretWord = secretWord;
		this.numberOfAttempts = 5;
	}

	@Override
	boolean openWithSecretNumber(int number, int attempt) {
		if (number == secretNumber && attempt < numberOfAttempts) {
			return true;
		}
		else {
		    return false;
		}
	}
	
	@Override
	boolean openWithSecretWord(String word) {
		if (word.equals(secretWord)) {
			return false;
		}
		else {
		    return true;
		}
	}
}
