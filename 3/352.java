class DecodeRobot {
    
    public void tap(String s, int x) {
       for(int i=0; i<x; i++)
		{
		  System.out.println(s);
		}
    }

    public void tap(String s) {
		  this.tap(s, 3);
    }

}

class Story {    
	public static void main(String[] args) {
	    DecodeRobot decodeRobot = new DecodeRobot();
        decodeRobot.tap("firstCode");
        decodeRobot.tap("secondCode", 5);
    }
}
